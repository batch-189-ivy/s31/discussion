const http = require('http')

const port = 4000

const server = http.createServer(function(request, response) {

	// You can use request.url to get the current destination of the user in the browser. You can then check if the current destination of the user matches with any endpoint and if it does, do the respective processes for each endpoint.
	if(request.url == '/greeting'){
		response.writeHead(200, {'Content-Type': 'text/plain'})
		response.end('Hello World')
	} else if (request.url == '/homepage'){
		response.writeHead(200, {'Content-Type': 'text/plain'})
		response.end('This is the homepage')
	} else {
		// If none of the endpoints match the current destination of the user, return a default value like 'Page not available'
		response.writeHead(404, {'Content-Type': 'text/plain'})
		response.end('Page not available. :(')
	}
})

server.listen(port)

console.log(`Server now accessible at localhost:${port}`)

