// 1. Import/require http from node.js. (http is what allows us to create a server)
let http = require('http')

// 2. Declare a variable assigned with a number value. This will be the port to be used by the server.
let port = 4000


// 3. Use http to create a server and write a header, then return data (like 'Hello World' string)
http.createServer(function(request, response) {

	// 4. Write the header for the reposnse specifying its content and status.
	response.writeHead(200, {'Content-Type': 'text/plain'})

	// 5. End the function and return a string with a 'Hello World'
	response.end('Hello World')
}).listen(port) // 6. Attach a port to the server specifying where you can access the server. (http://Localhost:4000)

// 7. Let the terminal/console know that the server is running and on which port
console.log(`Sever is running at localhost: ${port}`)


